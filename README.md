# AngularJS Website with Admin area boilerplate
It already includes categories, products registration and photo upload.

### Getting started
- make a clone of this repository `git clone https://recs182@bitbucket.org/recs182-boilerplates/full-wa.git project_name`
- delete .git `rm -rf .git`
- install packages `npm i`
- run gulp in a terminal
- see your project at `dist` folder, edit then in `app` folder