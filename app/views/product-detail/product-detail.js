class cProductDetail{
    constructor($stateParams, $q, $state, Post, ngMeta){
        this.stateParams = $stateParams;
        this.q = $q;
        this.state = $state;
        this.Post = Post;
        this.ngMeta = ngMeta;

        this.link = this.stateParams.link;
        this.idProduct = parseInt(this.stateParams.id);
        this.idCategory = Number(sessionStorage.productList_idCategory);

        this.product = {};
        this.categories = [];
        this.categoryTree = [];
        this.currentImageIndex = 0;

        this.init();
    }

    init(){
        const promises = [];
        promises.push( this.Post('products/select-by-id', {id_product: this.idProduct}) );
        promises.push( this.Post('categories/select-all') );

        this.q.all(promises).then(res => {
            const product = res[0];
            const categories = res[1];

            // product
            this.product = product.products;
            this.product.images = product.product_images;
            this.product.categories = product.product_categories;

            // category tree
            this.categories = categories;

            this.ngMeta.setTitle(`Produto ${this.product.reference}`);
            this.ngMeta.setTag('description', this.product.description);


            if(this.idCategory === 0){
                this.categoryTree.push({'name': 'Todos', 'link': 'todos', 'sort': 0});
            }else{
                this.buildCategoryTree(this.idCategory);
            }

        }, (err) => {
            let title = 'Ops! Ocorreu um problema...';
            let text = 'Não conseguimos carregar o produto que você solicitou.';

            switch(err.data.code){
                case 'PRODUCT_NOT_FOUND':
                    title = 'Produto não encontrado...';
                    text = 'O produto pode ter sido alterado ou removido.';
                    break;
            }

            swal({
                title: title,
                text: text,
                type: 'error',
                showConfirmButton: true
            }, (res) => {
                if(res) this.state.go('main.productList');
            });
        })
    }

    swipeImage(direction){
        const length = this.product.images.length;
        switch(direction){
            case 'right':
                if( this.currentImageIndex <= 0 ){
                    this.currentImageIndex = length - 1;
                }else{
                    this.currentImageIndex--;
                }
                break;
            case 'left':
                if( this.currentImageIndex >= (length - 1) ){
                    this.currentImageIndex = 0;
                }else{
                    this.currentImageIndex++;
                }
                break;
        }
    }

    buildCategoryTree(idCategory){
        const currentSerch = searchArray(this.categories, 'id', idCategory);
        this.categoryTree.push(currentSerch);

        if(Number(currentSerch.id_category) !== 0){
            this.buildCategoryTree(currentSerch.id_category);
        }
    }

}