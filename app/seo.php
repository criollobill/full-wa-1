<?php
session_start();
include('php/__api.php');
include('php/__connection.php');
include('php/__configs.php');

$user          = $_SERVER['HTTP_USER_AGENT'];
$url_extension = $_SERVER['REQUEST_URI'];

$title        = $_SESSION['CONFIGS']['COMPANY']['name'];
$description     = $_SESSION['CONFIGS']['COMPANY']['description'];

$image_fb = 'images/facebok.png';
$image_tw = 'images/twitter.png';

$url_data = explode('/' , $url_extension);
$extension = $url_data[1];

$bypass = @$_GET['bypass'];
if($bypass){
    $url_data = explode('/' , $bypass);
    $extension = $url_data[0];
}

switch($extension){
    case 'sitemap.xml':
        header('Content-type: application/xml');
        echo file_get_contents("sitemap.xml");
        die();
	case 'catalogo':
	    $category_link = end($url_data);

	    $result = SqlQuery("SELECT * from categories where link = '$category_link' and active = 1");
	    $category = $result[0];

		$title = 'Categoria: ' . $category['name'];
		$description = 'Você está os produtos da categoria '. $category['name'] . ' de nosso catálogo.';
		break;
    case 'produto':
        $product_id = $url_data[1];

        $result = SqlQuery("SELECT p.*, pi.image from products p left join product_images pi on pi.id_product = p.id where p.id = $product_id and p.active = 1 order by pi.sort limit 1");
        $product = $result[0];

        $title = 'Produto ' . $product['reference'];
        $description = $product['description'];

        $image_fb = $_SESSION['CONFIGS']['COMPANY']['URL']['base'] . 'images/products/' . $product['image'];
        $image_tw = $_SESSION['CONFIGS']['COMPANY']['URL']['base'] . 'images/products/' . $product['image'];
        break;
    case '404':
        $title = 'Página não encontrada. Erro 404';
        $description = 'Está página pode ter sido removida temporariamente ou removida.';
        break;
	default:
		$title = $_SESSION['CONFIGS']['COMPANY']['name'];
		$description = $_SESSION['CONFIGS']['COMPANY']['description'];
		break;
}
?>
<title><?php echo $title; ?></title>
<meta content="<?php echo $description ?>" name="description">
<meta content="bla, bla, bla, bla" name="keywords">
<meta content="pt-br" http-equiv="content-language">
<meta content="<?php echo $_SESSION['CONFIGS']['METATAGS']['geo_placename'];?>" name="geo.placename">
<meta content="<?php echo $_SESSION['CONFIGS']['METATAGS']['geo_region'];?>" name="geo.region">
<meta content="1 week" name="revisit-after">
<meta content="index, follow" name="robots">
<meta content="index, follow" name="googlebot">
<meta content="" name="author">
<meta content="<?php echo $_SESSION['CONFIGS']['COMPANY']['email'];?>" name="reply-to">
<link rel="canonical" href="<?php echo $_SESSION['CONFIGS']['COMPANY']['URL']['base'] . $url_extension; ?>">
<link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico">
<!-- og -->
<meta content="pt_BR" property="og:locale" />
<meta content="<?php echo $_SESSION['CONFIGS']['COMPANY']['URL']['base'] . $url_extension; ?>" property="og:url" />
<meta content="<?php echo $title; ?>" property="og:title" />
<meta content="<?php echo $title; ?>" property="og:site_name" />
<meta content="<?php echo $description; ?>" property="og:description" />
<meta content="<?php echo $image_fb;?>" property="og:image" />
<meta content="image/jpg" property="og:image:type" />
<meta content="800" property="og:image:width" />
<meta content="600" property="og:image:height" />
<meta content="website" property="og:type" />

<!-- twitter -->
<meta content="summary_large_image" name="twitter:card" />
<meta content="<?php echo $title; ?>" name="twitter:title" />
<meta content="<?php echo $description ?>" name="twitter:description" />
<meta content="<?php echo $image_tw;?>" name="twitter:image" />
