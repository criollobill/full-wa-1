class sEmail{
	constructor(Post, $q){
		return (to, template, vars = {}) => $q((resolve, reject) => {
			return Post('mailer/generic', {to, template, vars}).then(data => {
				const result = res;
				result.hasOwnProperty('error') ? reject(result) : resolve(result);
			}, err => {
				reject(err);
			});
		});
	}
}