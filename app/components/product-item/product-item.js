function ProductItemController(){
    this.images = this.product.images.split('!@!');
}

const ProductItem = {
    templateUrl: 'components/product-item/product-item.html',
    bindings: {
        product: '<',
    },
    controller: ProductItemController
};