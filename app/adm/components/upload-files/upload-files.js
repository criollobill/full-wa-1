// DIVIDE ARRAYS
function divideArrays(arr, divide){
    let splitted = Array();
    const times = Math.ceil(arr.length / divide);
    
    if(arr.length < divide){
        return [arr];
    }
    
    for(let counter = 0; counter < times; counter++){
        splitted.push(arr.splice(0, divide));
    }
    
    return splitted;
}

// REMOVE ACCENTS, ESPECIAL CHARACTERS, SPACES
function cleanFileReaderName(string){
    // remove accents
    const hexAccentsMap = {
        a : /[\xE0-\xE6]/g,
        A : /[\xC0-\xC6]/g,
        e : /[\xE8-\xEB]/g,
        E : /[\xC8-\xCB]/g,
        i : /[\xEC-\xEF]/g,
        I : /[\xCC-\xCF]/g,
        o : /[\xF2-\xF6]/g,
        O : /[\xD2-\xD6]/g,
        u : /[\xF9-\xFC]|ü/g,
        U : /[\xD9-\xDC]|Ü/g,
        c : /\xE7/g,
        C : /\xC7/g,
        n : /\xF1/g,
        N : /\xD1/g,
    };
    
    string = String(string);
    for(let character in hexAccentsMap){
        let regex = hexAccentsMap[character];
        string = string.replace(regex, character);
    }
    // remove spaces and specials
    return string.replace(/ |'|"|!|@|#|\$|%|¨|&|\*|\(|\)|=/gi, '');
}

// PROMISE FILEREADER, resolves async
function PromiseFileReader(file){
    return new Promise((resolve, reject) => {
        let reader = new FileReader();

        reader.addEventListener('load', () => {
            const fileName = cleanFileReaderName(file.name);
            resolve({base64: reader.result, name: fileName});
        }, false);

        reader.addEventListener('error', () => {
            reject('ERROR_FILE_READER');
        }, false);

        reader.readAsDataURL(file);
    })
}

function UploadFilesController($http, $uibModal, $q){
    this.http = http;
    this.uibModal = $uibModal;
    this.q = $q;

    this.isLoading = false;

    // UPLOAD FILES, onchange trigger
    this.upload = (input) => {
        this.isLoading = true;
        let promises = [];

        // get files
        const inputFiles = input.files;

        // iterate in files and push a Promise to each file
        for(let itemKey = 0; itemKey < inputFiles.length; itemKey++){
            promises.push(PromiseFileReader(inputFiles.item(itemKey)));
        }

        // execute all Promises at once
        this.q.all(promises).then(files => {
            const returnFile = files.map(file => file.name);

            const divide = this.divide ? Number(this.divide) : 100;
            const splitted = divideArrays(files, divide);
            const promises = splitted.map(split => this.http.post('api/upload/logged', {data: split, folder: this.folder, resize: this.resize}));
            
            this.q.all(promises).then(() => {
                // this.ngToast.create({content: 'Arquivo(s) carregado(s)'});
    
                //try/catch seem to be ignored by angular
                try{
                    if(this.multipleFiles){
                        this.ngModel = returnFile;
                    }else{
                        this.ngModel = returnFile[0];
                    }
                }catch(err){
                    throw 'ngModel is not defined in component uploadFiles. > ' + err;
                    // console.log('ngModel is not defined in component uploadFiles.');
                }
    
                this.isLoading = false;
            }, err => {
                this.isLoading = false;
                // this.ngToast.create({className: 'danger', content: 'Erro ao salvar arquivo(s)'});
                console.log('err', err);
            })
            
        }, err => {
            this.isLoading = false;
            // this.ngToast.create({className: 'danger', content: 'Erro ao processar arquivo(s)'});
            console.log('err', err);
        })
    };

    // returns default formats if accept not passed
    this.acceptFormats = () => {
        return this.accept === undefined ? '.jpg,.jpeg,.png,.gif' : this.accept;
    };
}

const UploadFiles = {
    templateUrl: 'components/upload-files/upload-files.html',
    bindings: {
        accept: '@',
        folder: '@',
        multipleFiles: '@',
        resize: '<',
        ngModel: '=',
        divide: '@'
    },
    controller: UploadFilesController,
};