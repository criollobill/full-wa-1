<?php
session_start();

include_once('../php/__configs.php');

function fileInclude($file){
	echo $file.'?cachetime=' . filemtime($file);
}

function getBase(){
    $http_host   = $_SERVER['HTTP_HOST'];
    $request_uri = $_SERVER['REQUEST_URI'];
    $http        = @$_SESSION['CONFIGS']['COMPANY']['URL']['ssl'] ? 'https://' : 'http://';
    $www         = substr($http_host, 0,3) == 'www' ? 'www.' : '';

    if(preg_match("/192\.168\.1|127\.0\.0|localhost/", $http_host)){
        if(preg_match("/192\.168\.1|127\.0\.0|localhost/", $http_host)){
            $joins = [];
            $splitted_url = explode('/', $request_uri);
            foreach($splitted_url as $splits){
                $joins[] = $splits;
                if(preg_match('/dist/', $splits)) break;
            }
            return 'http://' . $http_host . implode('/', $joins) . '/';
        }
        return 'http://'.$http_host .'/'.explode('/', $request_uri)[1].'/' . explode('/', $request_uri)[2] . '/';
    }else{
        if($_SESSION['CONFIGS']['APPROVAL']['active']){
            return $http . $http_host . str_replace('/adm', '', $request_uri);
        }
        return $http . $http_host .'/';
    }
}

if($_SESSION['CONFIGS']['COMPANY']['URL']['ssl'] && !@$_SERVER['HTTPS'] && !preg_match('/192\.168\.1|127\.0\.0|localhost/', $_SERVER['HTTP_HOST'])){
    header( 'location: https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] );
    die();
}
?>
<!DOCTYPE html>
<html ng-app="app">
<head>
<base href="<?php echo getBase(); ?>adm/" />

<title>Painel Administrativo</title>

<meta content="description" name="Painel Administrativo">
<meta content="Keywords" name="keywords">

<meta charset="utf-8">
<meta content="IE=edge" http-equiv="X-UA-Compatible">
<meta content="width=device-width, initial-scale=1, user-scalable=no" name="viewport">
<meta content="pt-br" http-equiv="content-language">

<meta content="noindex, nofollow" name="robots">
<meta content="noindex, nofollow" name="googlebot">

<link rel="shortcut icon" type="image/x-icon" href="../images/favicon.ico">

<!-- css -->
<link rel="stylesheet" type="text/css" href="<?php fileInclude('css.css'); ?>">
</head>
<body ng-class="{'scroll_blocked': scroll.blocked}">
<toast></toast>
<?php
if(!@$_SESSION['administrator_logged']){
    require_once('views/login/login.html');
}else{
?>
    <ui-view></ui-view>
<?php
}
?>
<script type="text/javascript" src="<?php fileInclude('../libs.js'); ?>"></script>
<script type="text/javascript" src="<?php fileInclude('js.js'); ?>"></script>
<script type="text/javascript" src="<?php fileInclude('../const.php'); ?>"></script>
</body>
</html>
