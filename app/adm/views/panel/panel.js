class cPanel{

    constructor(Post){
        this.Post = Post;

        this.forms = [];

        this.init();
    }

    init(){
        this.Post('form/select-all').then(res => {
            res.forEach(row => {
                row.form = JSON.parse(row.form);
            });
            this.forms = res;
        }, err => {
            console.log(err);
        })
    }

    deleteNews(id){
        this.Post('form/delete', {id: id}).then(() => {
            this.init();
            swal({ title: 'Email removido', text: 'O email foi removido com sucesso', type: 'success', showConfirmButton: true });
        }, () => {
            swal({ title: 'Falha ao remover', text: 'Tente novamente em alguns instantes', type: 'error', showConfirmButton: true });
        })
    }
}