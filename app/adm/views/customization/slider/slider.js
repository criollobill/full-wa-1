class cCustomizationSlider{
    constructor(Post, $uibModal){
        this.Post = Post;
        this.uibModal = $uibModal;

        this.loaded = false;
        this.sliders = [];
        this.new = {image: '', name: ''};

        this.init();
    }

    init(){
        this.Post('slider/select-all').then(res => {
            this.sliders = res;
            this.loaded = true;
        }, err => {
            console.log('create:', err);
        })
    }

    create(){
        // inject creation date, sort number
        this.new.creation = new Date().getTime();
        this.new.sort = this.sliders.length;

        if(!this.new.image){
            // this.ngToast.create({className: 'danger', content: 'Escolha uma imagem antes de cadastrar'});
            return;
        }

        this.Post('slider/multi', {data: this.new}).then(() => {
            this.new = {image: '', name: ''};
            this.init();
        }, err => {
            console.log('create:', err);
        })
    }

    delete(slider){
        const confirmation = confirm(`Tem certeza que deseja deletar o slider ${slider.name}?`);

        if(confirmation){
            this.Post('slider/multi', {data: {id: slider.id, active: 0 }}).then(() => {
                // this.ngToast.create({className: 'info', content: 'Slider removido'});
                this.init();
            }, err => {
                // this.ngToast.create({className: 'danger', content: 'Erro ao remover slider'});
                console.log('remove:', err);
            })
        }
    }

    imageZoom(image){
        this.uibModal.open({
            templateUrl: 'views/customization/slider/image-zoom/image-zoom.html',
            controller: 'cCustomizationSliderImageZoom',
            controllerAs: '$ctrl',
            size: 'lg',
            resolve: {
                data: () => image
            },
        });
    }
}