class cProductCategoriesOrder{

    constructor($rootScope, $uibModalInstance, Post, data){
        this.rootScope = $rootScope;
        this.uibModalInstance = $uibModalInstance;
        this.Post = Post;

        this.data = data;
        this.sortableMode = 'on';
    }

    confirm(){

        const update = this.data.levels.map((value, index) => {
            return {
                id: value.id,
                sort: index,
            }
        });

        this.Post('categories/edit', update).then(() => {
            // this.ngToast.create({content: '<i class="fa fa-check"></i> Categorias ordenadas'});
            this.rootScope.$broadcast('REORDER_CATEGORIES', this.data.currentLevel);
            this.close();
        }, err => {
            console.log('confirm: ' , err);
        })
    }

    close(){
        this.uibModalInstance.close();
    }

}
