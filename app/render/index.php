<?php
function url(){
	$http_post   = @$_SERVER['HTTP_HOST'];
	$request_uri = @$_SERVER['REQUEST_URI'];
	$protocol    = @$_SERVER['HTTPS'] ? 'https://' : 'http://';
	$domain      = str_replace('www.', '', $http_post);

	return $page = $protocol . $domain . $request_uri;
}
$page = url();

if($page){
	$phantomjs = strtoupper(substr(PHP_OS, 0, 3)) === 'WIN' ? 'phantomjs.exe' : 'phantomjs';
	$regex     = '/\<\!DOCTYPE html\>.*/mi';

	$content = shell_exec("$phantomjs --ssl-protocol=any --ignore-ssl-errors=true --web-security=false render.js $page");
	if( preg_match($regex, $content, $matches) ){
		echo $matches[0];
	}
}