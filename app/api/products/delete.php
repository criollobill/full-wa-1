<?php
require_once('../php/logged.php');

isAdmLogged();

$id_product = @$_POST['id_product'];

if(!$id_product){
    return Error('ID_PRODUCT_NULL');
}

$delete = array("id" => $id_product, "active" => 0);
return SqlInsert('products', $delete);