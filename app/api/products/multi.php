<?php
require_once('../php/logged.php');
isAdmLogged();

$products = @_POST['products'];
$product_images = @_POST['product_images'];
$product_categories = @_POST['product_categories'];

$id_product = SqlInsert('products', $products);

// IMAGES
if(count($product_images)){
    // delete all created images
    SqlQuery("DELETE FROM product_images WHERE id_product = $id_product");

    // ensure id_product is set
    foreach ($product_images as &$image){
        $image['id_product'] = $id_product;
    }

    SqlInsert('product_images', $product_images);
}

// CATEGORIES
if(count($product_categories)){
    // delete all created categories
    SqlQuery("DELETE FROM product_categories WHERE id_product = $id_product");

    $categories = array();
    foreach ($product_categories as $id_category){
        $category = array("id" => 0, "id_product" => $id_product, "id_category" => $id_category);
        array_push($categories, $category);
    }
    SqlInsert('product_categories', $categories);
}
return $id_product;